import 'isomorphic-fetch'
import createEvent from './../helpers/createEvent'
import { store, history } from './../helpers/bootstrap'

// Testable
export const unboundOnClickSubmit = (e, { store, history }) => {
  return fetch('/public/stub.json').then(response => response.json()).then((stub) => {
    if (stub.area === store.getState().area) {
      store.setState({listings: stub.listing})
      store.setState({isListingsReady: true})
      history.push('/results')
    } else {
      store.setState({isListingsReady: false})
    }
  })
}

export const unboundOnChangeArea = (e, { store }) => {
  store.setState({area: e.target.value })
}

// For use
export const onClickSubmit = createEvent('click', unboundOnClickSubmit, { store, history})

export const onChangeArea = createEvent('change', unboundOnChangeArea, { store })

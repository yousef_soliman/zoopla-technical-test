import fetchMock from 'fetch-mock'
import sinon from 'sinon'
import { expect } from 'chai'
import createEvent from './../helpers/createEvent'
import { store, history } from './../helpers/bootstrap'
import { unboundOnClickSubmit, unboundOnChangeArea } from './events'

describe('Core: Events - onClickSubmit', () => {

  afterEach(() => {
    fetchMock.restore()
  })

  it('Calling unboundOnClickSubmit, if state area is the same as stub area, then call store.setState with listings, then call history.push with "/results" as args', () => {
    const store = { getState: () => ({ area: 'N11' }), setState: sinon.spy() }
    const history = { push: sinon.spy() }

    fetchMock.mock('/public/stub.json', {
      status: 200,
      body: {
        area: 'N11',
        listing: []
      }
    })

    return unboundOnClickSubmit({}, { store, history }).then(() => {
      expect(store.setState.calledWith({listings: []})).to.be.true
      expect(store.setState.calledWith({isListingsReady: true})).to.be.true
      expect(history.push.calledWith('/results')).to.be.true
    })
  })

  it('Calling unboundOnClickSubmit, if state area is not the same as stub area, then call store.setState with isListingsReady false as arg', () => {
    const store = { getState: () => ({ area: 'N11' }), setState: sinon.spy() }
    const history = { push: sinon.spy() }

    fetchMock.mock('/public/stub.json', {
      status: 200,
      body: {
        area: 'SE1',
        listing: []
      }
    })

    return unboundOnClickSubmit({}, { store, history }).then(() => {
      expect(store.setState.calledWith({isListingsReady: false})).to.be.true
      expect(history.push.called).to.be.false
    })
  })
})


describe('Core: Events - onChangeArea', () => {
  it('Calling unboundOnChangeArea, call store.setState with area === e.target.value as args', () => {
    const store = { setState: sinon.spy() }
    unboundOnChangeArea({target: { value: 'N11' }}, { store })
    expect(store.setState.calledWith({area: 'N11'})).to.be.true
  })
})
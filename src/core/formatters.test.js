import { expect } from 'chai'
import { listingFormatters, listingFormattersApply } from './formatters'

describe('Core: Formatters - listingFormatters', () => {
  it('Price formatter returns price in the format: "£0,000,000" or "£0,000,000" given price', () => {
    expect(listingFormatters.price(1800000)).to.equal("£1,800,000")
    expect(listingFormatters.price(975000)).to.equal("£975,000")
  })

  it('Title formatter returns title in the format: "<bedroom number> bedroom <property type> for sale" given bedroom number and property type', () => {
    expect(listingFormatters.title(5, 'detached house')).to.equal('5 bedroom detached house for sale')
  })

  it('Agent address formatter returns agent addressin the format: "1 Longfield Ave, N11 9AH" given agent address and agent postcode', () => {
    expect(listingFormatters.agentAddress('1 Longfield Ave', 'N11 9AH')).to.equal('1 Longfield Ave, N11 9AH')
  })

  it('Agent phone formatter returns agent phone in the format: "T: <agent phone number>" given agent phone number', () =>{
    expect(listingFormatters.agentPhone('0208993103')).to.equal('T: 0208993103')
  })
})

describe('Core: Formatters - listingFormattersApply', () => {
  let listing = {
    price: 1800000,
    num_bedrooms: 5,
    property_type: 'detached house',
    agent_address: '1 Longfield Ave',
    agent_postcode: 'N11 9AH',
    agent_phone: '02083394001'

  }

  let $ = {
    price: listingFormatters.price(listing.price),
    title: listingFormatters.title(listing.num_bedrooms, listing.property_type),
    agentAddress: listingFormatters.agentAddress(listing.agent_address, listing.agent_postcode),
    agentPhone: listingFormatters.agentPhone(listing.agent_phone)
  }

  let formattedListing = Object.assign({}, listing, {$})


  it('Calling listingFormattersApply([listing], ...listingFormatters) returns an array, for which each value contains a $ object with correctly formatted data', () => {
    let newFormattedListing = listingFormattersApply([listing], {...listingFormatters})[0]
    expect(newFormattedListing).to.eql(formattedListing)
  })
})
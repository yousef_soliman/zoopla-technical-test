/**
 * Formatters for injection into formatter service.
 * @type {Object}
 */
export const listingFormatters = {
  price: (value) => `£${parseInt(value).toLocaleString()}`,
  title: (bedroomCount, propertyType) => `${bedroomCount} bedroom ${propertyType} for sale`,
  agentAddress: (agentAddress, agentPostcode) => `${agentAddress}, ${agentPostcode}`,
  agentPhone: (agentPhone) => `T: ${agentPhone}`
}

/**
 * Formatter to apply via formatter service. 
 * @param  {[type]} data                 Data that will be passed by formatter.apply
 * @param  {[type]} options.price        [description]
 * @param  {[type]} options.title        [description]
 * @param  {[type]} options.agentAddress [description]
 * @param  {[type]} options.agentPhone   [description]
 * @return {[type]}                      [description]
 */
export const listingFormattersApply = (data, { price, title, agentAddress, agentPhone }) => {
  return data.map((item) => {
    let $ = {
      price: price(item.price),
      title: title(item.num_bedrooms, item.property_type),
      agentAddress: agentAddress(item.agent_address, item.agent_postcode),
      agentPhone: agentPhone(item.agent_phone)
    }

    return Object.assign({}, item, {$})
  })
}

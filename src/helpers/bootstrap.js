import createStore from './createStore'
import createHistory from './createHistory'
import createFormatter from './createFormatter'
import createRenderer from './createRenderer'
import { listingFormatters } from './../core/formatters'

const initialState = {
  listings: [],
  isListingsReady: true,
  area: ''
}

export const store = createStore(initialState)

export const history = createHistory(store)

export const formatter = createFormatter(listingFormatters)

export const renderer = createRenderer({rootElement: document.getElementById('app')})

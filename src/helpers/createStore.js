/**
 * Used for storing application state, implementing Observable/Revealing Module pattern.
 * @param  {Object} initialState Initial state of store.
 * @return {Object}              Exposed methods.
 */
export const createStore = (initialState = {}) => {
  let state = initialState
  let subscriptions = []

  /**
   * Subscribe to state changes, key arg left for extending to unsubscribe, but we're not using it here.
   * @param  {[type]}   filter [description]
   * @param  {Function} next   [description]
   * @param  {[type]}   key    [description]
   * @return {[type]}          [description]
   */
  const subscribe = (filter, next, key = null) => {
    let subscription = { filter, next }
    if (key !== null) {
      subscription.key = key
    }
    subscriptions.push(subscription)
  }

  const getSubscriptions = () => subscriptions

  const getState = () => state

  const setState = (newState) => {
    let oldState = state
    let nextState = Object.assign({}, oldState, newState)
    
    state = nextState

    subscriptions.forEach((subscription) => {
      if (subscription.filter(oldState, nextState)) {
        subscription.next()
      }
    })
  }

  return {
    getState,
    setState,
    subscribe,
    getSubscriptions
  }
}

export default createStore
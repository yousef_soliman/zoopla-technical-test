/**
 * Generate events on the fly, injecting services as dependencies
 * @param  {[type]}   on           [description]
 * @param  {Function} fn           [description]
 * @param  {[type]}   dependencies [description]
 * @return {[type]}                [description]
 */
export const createEvent = (on, fn, dependencies) => {
  return (element) => {
    element.addEventListener(on, (e) => {
      fn(e, {...dependencies})
    })
  }
}

export default createEvent
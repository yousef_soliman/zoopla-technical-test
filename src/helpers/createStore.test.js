import { expect } from 'chai'
import sinon from 'sinon'
import { createStore } from './createStore'

const mockSubscriber = {
  filter: (state, newState) => state.thisFlag !== newState.thisFlag,
  next: sinon.spy()
}

const mockState = {thisFlag: true, thatFlag: false, thisArray: []}

describe('Factory: createStore', () => {
  it('Calling createStore() exposes getState, setState and suscribe methods', () => {
    let store = createStore()
    expect(store.getState).to.be.a('function')
    expect(store.setState).to.be.a('function')
    expect(store.subscribe).to.be.a('function')
  })

  it('Calling store.getState() returns current state', () => {
    let store = createStore(mockState)
    expect(store.getState()).to.eql(mockState)
  })

  it('Calling store.setState() updates the state', () => {
    let store = createStore(mockState)
    store.setState({thisFlag: false})

    expect(store.getState().thisFlag).to.be.false
  })

  it('Calling store.subscribe() adds a subscribe listener to the store', () => {
    let store = createStore(mockState)
    store.subscribe(mockSubscriber.filter, mockSubscriber.next)
    expect(store.getSubscriptions()[0]).to.eql(mockSubscriber)
  })

  it('When store.setState() is called, any subscription next methods are called if their filter method evalutes to true', () => {
    let store = createStore(mockState)
    store.subscribe(mockSubscriber.filter, mockSubscriber.next)
    // This should call mockSubsriber.next
    store.setState({thisFlag: false})
    expect(mockSubscriber.next.calledOnce).to.be.true
    // This shouldn't call mockSubscriber.next as mockSubscriber.filter() should evaluate to false
    store.setState({thatFlag: true})
    expect(mockSubscriber.next.calledOnce).to.be.true
  })
})
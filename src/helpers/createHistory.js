/**
 * Used to access HTML5 History Web API while tapping into createStore() state managment.
 * @param  {Object} store [description]
 * @return {[type]}       [description]
 */
export const createHistory = (store = {}) => {
  // Log error to console if expected depedency doesn't contain required methods
  if (store.getState === undefined || store.setState === undefined) {
    console.error(store, 'Expected store to have getState() and setState() methods.')
  }

  // Pick history off of window.history
  const { history } = window

  /**
   * Set _historyLocation in state
   * @param  {[type]} value [description]
   * @return {[type]}       [description]
   */
  const setStoreHistoryState = (value = null) => {
    let _historyLocation = value || window.location.pathname
    store.setState({_historyLocation})
  }
  /**
   * Abstraction for window.history.pushState()
   * @param  {[type]} value [description]
   * @return {[type]}       [description]
   */
  const push = (value) => {
    setStoreHistoryState(value)
    history.pushState(store.getState(), null, value)
  }

  /**
   * Abstraction for window.history.replaceState()
   * @param  {[type]} value [description]
   * @return {[type]}       [description]
   */
  const replace = (value) => {
    setStoreHistoryState(value)
    history.replaceState(store.getState(), null, value)
  }

  /**
   * For use with store subscriber as a filter function which returns true/false.
   * @param  {[type]} state    [description]
   * @param  {[type]} newState [description]
   * @return {[type]}          [description]
   */
  const onChangeFilter = (state, newState) => {
    return newState.hasOwnProperty('_historyLocation') && state._historyLocation !== newState._historyLocation
  }

  /**
   * Check if given needle matches the current location stored in state.
   * @param  {[type]} needle [description]
   * @return {[type]}        [description]
   */
  const matches = (needle) => needle === store.getState()._historyLocation

  /**
   * Event handler for onload
   * @return {[type]} [description]
   */
  window.onload = () => {
    setStoreHistoryState()
  }

  /**
   * Event handler for onpopstate (when user invokes browser action e.g. back button)
   * @return {[type]} [description]
   */
  window.onpopstate = () => {
    setStoreHistoryState()
  }

  /**
   * Interface: push, replace, matches, onChangeFilter
   */
  return {
    push,
    replace,
    matches,
    onChangeFilter
  }
}

export default createHistory
/**
 * Used to create text formatters.
 * @return {[type]} [description]
 */
export const createFormatter = (formatters = {}) => {
  /**
   * Register a new formatter.
   * @param  {[type]}   key [description]
   * @param  {Function} fn  [description]
   * @return {[type]}       [description]
   */
  const register = (key, fn) => {
    formatters[key] = fn
  }

  /**
   * Get formatters.
   * @return {[type]} [description]
   */
  const getFormatters = () => formatters

  /**
   * Apply stored formatters, {...formatters} allows formatters stored to be invoked by name.
   * @param  {[type]}   data [description]
   * @param  {Function} fn   [description]
   * @return {[type]}        [description]
   */
  const apply = (data, fn) => {
    return fn(data, {...formatters})
  }

  /**
   * Interface: register, apply
   */
  return {
    register,
    apply,
    getFormatters
  }
}

export default createFormatter
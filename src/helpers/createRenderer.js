import Mustache from 'mustache'
import 'isomorphic-fetch'

const defaultContext = {rootElement: null, formatters: [], method: 'fetch'}

/**
 * Creates a render that allows for fetching HTML templates, passing them through Mustache and injecting them into the DOM.
 * @param  {[type]} context [description]
 * @return {[type]}         [description]
 */
export const createRenderer = (context = defaultContext) => {
  // As default arg for createRenderer is supplied as object a single property supplied at initialisation
  // will result in the others not being present in the instance, therefore we extend the default arg against
  // supplied context.
  let { rootElement, formatters, method } = Object.assign({}, defaultContext, context)

  /**
   * Handle using Fetch.
   * @param  {[type]} templatePath [description]
   * @return {[type]}              [description]
   */
  const getTemplateWithFetch = (templatePath) => {
    return fetch(templatePath)
    .then(response => {
      if (response.ok) {
        return response.text()
      } else {
        throw new Error()
      }
    })
  }

  /**
   * Pipe into Mustache.render and inject into DOM.
   * @param  {[type]} element [description]
   * @param  {[type]} raw     [description]
   * @param  {[type]} data    [description]
   * @return {[type]}         [description]
   */
  const render = (element, templateString, data) => {
    element.innerHTML = Mustache.render(templateString, data)
  }

  /**
   * Abstraction and interface for createRenderer()
   * @param  {[type]}   templatePath  [description]
   * @param  {[type]}   data          [description]
   * @param  {[type]}   parentElement [description]
   * @param  {Function} after         [description]
   * @return {[type]}                 [description]
   */
  const template = (templatePath, data, parentElement = null, after = () => {}) => {
    let element = parentElement || rootElement

    if (method === 'fetch') {
      getTemplateWithFetch(templatePath).then((templateString) => {
        render(element, templateString, data)
        after()
      })
    }
  }

  /**
   * Get private functions for testing, can be stripped from here and factory interface in production (e.g. using process.env.NODE_ENV)
   * @return {[type]} [description]
   */
  const getPrivates = () => {
    return {
      methods: {
        render,
        getTemplateWithFetch
      },
      properties: {
        rootElement,
        formatters,
        method
      }
    }
  }

  /**
   * Interface: template, getPrivates
   */
  return {
    template,
    getPrivates
  }
}

export default createRenderer
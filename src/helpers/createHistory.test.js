import { expect } from 'chai'
import sinon from 'sinon'
import { createHistory } from './createHistory'
import { createStore } from './createStore'

// Allow for this to be tested without need for History Web API (e.g. without DOM)
const windowHistory = window.history || { pushState: () => {}, replaceState: () => {}}

// Dependency injectable for createHistory()
const store = createStore()

describe('Factory: createHistory', () => {
  // Mock window.history method calls
  beforeEach(() => {
    window.history.pushState = sinon.spy()
    window.history.replaceState = sinon.spy()
  })
  // Restore window.history to JS default
  afterEach(() => {
    window.history.pushState = windowHistory.pushState
    window.history.replaceState = windowHistory.replaceState
  })

  it('Calling createHistory exposes push, replace, matches and onChangeFilter methods', () => {
    let history = createHistory(store)

    expect(history.push).to.be.a('function')
    expect(history.replace).to.be.a('function')
    expect(history.matches).to.be.a('function')
    expect(history.onChangeFilter).to.be.a('function')
  })

  it('Calling history.push(location) should result in store containing _historyLocation: location and window.history.pushState being called', () => {
    let history = createHistory(store)
    history.push('/newPage')
    expect(store.getState()._historyLocation).to.equal('/newPage')
    expect(window.history.pushState.called).to.be.true
  })

  it('Calling history.replace(location) should result in store containing _historyLocation: location and window.history.replaceState being called', () => {
    let history = createHistory(store)
    history.replace('/newPage')
    expect(store.getState()._historyLocation).to.equal('/newPage')
    expect(window.history.replaceState.called).to.be.true
  })

  it('Calling history.matches(location) should evaulate to true if the value of _historyLocation in store matches', () => {
    let history = createHistory(store)
    history.push('/newPage')
    expect(history.matches('/newPage')).to.be.true
    expect(history.matches('/oldPage')).to.be.false
  })

  it('Calling history.onChangeFilter(oldState, newState) should evaluate to true if the _historyLocation props of either args not equal ===', () => {
    let oldState = {_historyLocation: 2}
    let newState = {_historyLocation: "2"}

    let history = createHistory(store)

    expect(history.onChangeFilter(oldState, newState)).to.be.true
    expect(history.onChangeFilter(oldState, {_historyLocation: 2})).to.be.false
  })
})
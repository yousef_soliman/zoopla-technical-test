import { expect } from 'chai'
import { createFormatter } from './createFormatter'

const mockData = [
  { title: 'Title #1', price: '1800000' },
  { title: 'Title #2', price: '975000' }
]

const mockFormatters = {
  title: (value) => `Title is ${value}`,
  price: (value) => `Price is ${value}`
}

const mockAppliedFormatter = (data, { title, price }) => {
  expect(title).to.eql(mockFormatters.title)
  expect(price).to.eql(mockFormatters.price)
  expect(data).to.eql(mockData)
}

describe('Factory: createFormatter', () => {
  const formatter = createFormatter(mockFormatters)
  
  it('formatter exposes register/apply/getFormatters methods', () => {
    expect(formatter.register).to.be.a('function')
    expect(formatter.apply).to.be.a('function')
    expect(formatter.getFormatters).to.be.a('function')
  })

  it('Calling formatter.getFormatters() returns stored formatting functions (empty object by default)', () => {
    expect(formatter.getFormatters()).to.eql(mockFormatters)
  })

  it('Calling formatter.register(key, func) registers formatting function', () => {
    let func = () => ''
    formatter.register('test', func)
    expect(formatter.getFormatters().test).to.equal(func)
  })

  it('Calling formatter.apply(data, function(...)) exposes data and all functions by name to function(...)', () => {
    formatter.apply(mockData, mockAppliedFormatter)
  })

})
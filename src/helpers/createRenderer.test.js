import { expect } from 'chai'
import sinon from 'sinon'
import fetchMock from 'fetch-mock'
import Mustache from 'mustache'
import { createRenderer } from './createRenderer'

const onFulfilledSpy = sinon.spy()

const defaultContext = {rootElement: null, formatters: [], method: 'fetch'}

/**
 * Because of a large amount of abstraction in this factory, I've exposed all private methods which handle
 * that abstraction for testing. Even though these are implementation details, exposing them for testing
 * makes for much easier clean tests without a lot of config/tear-down.
 */
describe('Factory: createRenderer', () => {

  afterEach(() => {
    fetchMock.restore()
  })
  it('Calling createRenderer exposes template method', () => {
    let renderer = createRenderer()
    expect(renderer.template).to.be.a('function')
  })

  it('Calling createRenderer with no args results in rootElement, formatters and method properties existing on the object equal to defaultContext', () => {
    let properties = createRenderer().getPrivates().properties
    expect(properties).to.eql(defaultContext)
  })

  it('Calling createRenderer with arguments overrides any property in the defaultContext when setting rootElement, formatters and method properties', () => {
    let properties = createRenderer({rootElement: 'body'}).getPrivates().properties
    expect(properties).to.eql(Object.assign({}, defaultContext, {rootElement: 'body'}))
  })

  it('Calling renderer getTemplateWithFetch(url, onFulfilled) <private> calls onFulfilled with response text if fetch promise fulfilled', () => {
    let getTemplateWithFetch = createRenderer().getPrivates().methods.getTemplateWithFetch

    fetchMock.mock('/templateUrl', {
      status: 200,
      body: 'This is the body'
    })

    return getTemplateWithFetch('/templateUrl').then((text) => {
      expect(text).to.equal('This is the body')
    })
  })

  it('Calling renderer with render(element, templateString, data) <private> passes the templateString and data through Mustache.render and sets the innerHTML property on element', () => {
    let render = createRenderer().getPrivates().methods.render

    let data = {title: 'This is the body'}
    let templateString = '<h2>{{title}}></h2>'
    let mockElement = {innerHTML: ''}

    render(mockElement, templateString, data)

    expect(mockElement.innerHTML).to.equal(Mustache.render(templateString, data))
  })
})
// Helper services
import { store, history, formatter, renderer } from './helpers/bootstrap'
// Core
import { listingFormattersApply } from './core/formatters'
import { onClickSubmit, onChangeArea } from './core/events'

// Create a new filter, using history.onChangeFilter coupled with a check on state change to 'isListingsReady'
const renderSubscriberFilter = (oldState, newState) => {
  return history.onChangeFilter(oldState, newState) || oldState.isListingsReady !== newState.isListingsReady
}

// Our only subscriber, callback fires whenever renderSubscriberFilter() evaluates to true
store.subscribe(renderSubscriberFilter, () => {
  // Let's get the current state elements we want for our rendering
  let { listings, isListingsReady, area } = store.getState()

  // Our routing
  if (history.matches('/search')) {
    // If the value of _historyLocation within state is /, render this
    renderer.template('/public/views/search.html', { listings, isListingsReady, area }, document.getElementById('app'), () => {
      // Event binding to elements which are now in the DOM
      onClickSubmit(document.getElementById('submit'))
      onChangeArea(document.getElementById('input'))
    })
  } else if (history.matches('/results')) {
    // Apply the formatter listingsFormattersApply using listings as data
    let formattedListings = formatter.apply(listings, listingFormattersApply)
    renderer.template('/public/views/results.html', { listings: formattedListings }, document.getElementById('app'), () => {

    })
  }

})
## Zoopla Technical Test

Please find the completed technical test for Zoopla in this repository.

## Usage

Clone down the repository and then run the following commands locally within the cloned folder:

### Installing Dependencies

There's some dependencies that need to be installed, so run:

```
npm install
```

### Serving the project

Run the following command locally in your CLI:

```
npm run serve
```

Open your browser and visit the following URL:

```
http://localhost:3000/search
```

### Test cases

Run the following command locally in your CLI:

```
npm run build:hot-reload:tests
```

Open your browser and visit the following URL:

```
http://localhost:3032/tests.html
```

## Strategy Overview

This has been built using ES6 with Babel and Webpack to transpile/compile. I've built this as a single page app, with a variety of different services that are bootstrapped and then injected as dependencies where needed e.g. history service takes store service as a dependency.

The entry file is src/app.js.

## What Would I Add

Given more time I would have done the following:

- Modularised my CSS, likely using CSS Modules - or at the very least used a pre-processor like LESS/SASS so that I don't have a tonne of classes polluting the "global" style namespace
- Written tests for the Express routes


## Edge Cases

The requirement spec didn't comment on how to specifically handle the following edge cases, so I've detailed my handling below:

- A request to the root path (/) will result in a 404 page being served as per the requirement spec that every page request other than /search and /results should receive a 404 response
- A direct request to /results will result in a redirect to /search
- On clicking the submit button, the given value in the text input is matched against the area property in public/stub.json, and is case-sensitive, e.g. only "N11" is valid not "n11"

## Feedback

I completed this test in around 5 hours, although be it with a fair few breaks. This is definitely the most interesting and qualifiably well rounded techincal test I have done.
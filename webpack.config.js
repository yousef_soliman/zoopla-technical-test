var webpack = require('webpack')
var path = require('path')

var moduleLoaders = [
  // Babel transpile to ES5
  {
    test: /\.js$/,
    exclude: /node_modules/,
    loader: 'babel-loader',
    options: {
      presets: ['es2015', 'stage-2']
    }
  },
  // Support for CSS Modules
  {
    test: /\.css$/,
    use: ['style-loader', 'css-loader']
  }
];

module.exports = {
  devtool: 'source-map',
  entry: [
      'babel-polyfill',
      './src/app.js'
  ],
  output: {
    path: path.join(__dirname, 'public'),
    filename: 'bundle.js',
    publicPath: 'public/'
  },
  resolve: {
    modules: ['node_modules', 'src/'],
    extensions: ['.js'],
  },
  module: {
    rules: moduleLoaders
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin()
  ],
  devServer: {
    port: 3031,
    hotOnly: true
  },
  node: {
    fs: 'empty'
  }
}
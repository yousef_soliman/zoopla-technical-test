var express = require('express')
var path = require('path')

var app = express()

app.use('/public', express.static(path.join(__dirname, 'public')))

app.get('/search', function(req, res) {
  res.sendFile(path.join(__dirname, 'index.html'))
})

// If someone attempts to access /results directly, redirect to /search
// as this is built as a single page app, but no state is held on the server side e.g. no listings
// as a succesful search hasn't been performed.
app.get('/results', function(req, res) {
  res.redirect('/search')
})

// For any other requests, send back a 404
app.get('*', function(req, res) {
  res.status(404).send('Requested page not found.')
})

app.listen(3000, function() {
  console.log('Listening on port 3000, access via http://localhost:3000/search')
})